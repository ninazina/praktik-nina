<?php
echo 'задание 1-If <br> ';
$age=40;
if ($age<18) {
    echo 'Вы несовершеннолетний';
} elseif ($age>=18 and $age<65) {
    echo 'Вы взрослый';
} else {
    echo 'Вы пенсионер';
}
echo '<br>';

echo 'задание 2-If <br> ';
$a=8;
if ($a%2==0){
    echo 'Число является четным'; 
} else {
    echo 'Число является нечетным'; 
};
echo '<br>';

echo 'задание 1-switch <br> ';
$dayOfWeek = 5;
switch ($dayOfWeek){
  case '1';
  echo 'понедельник';
  break;
  case '2';
  echo 'вторник';
  break;
  case '3';
  echo 'среда';
  break;
  case '4';
  echo 'четверг';
  break;
  case '5';
  echo 'пятница';
  break;
  case '6';
  echo 'суббота';
  break;
  case '7';
  echo 'воскресенье';
  break;
}
echo '<br>';

echo 'задание 2-switch <br> ';
$month=5;
switch ($month){
    case '2';
    echo 'В этом месяце 28 дней';
    break;
    case '4';
    case '6';
    case '9';
    case '11';
    echo 'В этом месяце 30 дней';
    break;
    default:
    echo 'В этом месяце 31 день';
    break;
}
echo '<br>';

echo 'задание 1-match <br> ';
$string = "level";
$result=match(strrev($string)) {
    "level"=> 'Строка является палиндромом',
   default => 'Строка не является палиндромом',
};
echo (($result));
echo '<br>';

echo 'задание 2-match <br> ';
$number = 15;
$result = match (true) {
	$number%2==0 => 'Четное',
    $number%3==0 => 'кратное 3',
    $number%5==0 => 'кратное 5',
};
echo $result;
echo '<br>';


echo 'задание 1-while <br> ';
$num = 10;
$sum = 0;
$i = 0;
while ($i<$num) {
    if ($i%2==0){
    $sum+=$i;
    }
 $i++;
}
echo $sum;
echo '<br>';


echo 'задание 2-while <br> ';
$i = 1;
while (true) {
    if ($i%7==0){
    echo $i;
    break;
    }
 $i++;
}
echo '<br>';


echo 'задание 1-FOR <br> ';
$numbers = [1, 2, 3, 4, 5];
$sum = 0;
for ($i=0; $i< count($numbers);$i++) {
    $sum+=$numbers[$i];
}
echo $sum;
echo '<br>';


echo 'задание 2-FOR <br> ';
$numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
$evenNumbers = [];
for ($i=0; $i< count($numbers);$i++) {
    if ($numbers[$i]%2==0) {
        $evenNumbers[$i]=$numbers[$i];
    }
}
print_r ($evenNumbers);
echo '<br>';


echo 'задание 1-Foreach <br> ';

$info = ['name'=>'Olga','age'=>'20'];
$info ['status'] = 'married';
foreach ($info as $value){
}
print_r ($info);
echo '<br>';


echo 'задание 2-Foreach <br> ';
$first = ['name'=>'Olga','age'=>'20'];
$second = ['secondname'=>'Rudko','city'=>'Minsk'];
$person= array_merge ($first, $second);
foreach ($person as $key=>$value){
}
print_r ($person);
echo '<br>';

?>




